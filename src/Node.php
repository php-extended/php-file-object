<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\File;

use InvalidArgumentException;

/**
 * Node class file.
 * 
 * This class represents a node of the tree in the filesystems.
 * 
 * @author Anastaszor
 */
abstract class Node implements NodeInterface
{
	
	/**
	 * The current filesystem to which this node is attached.
	 * 
	 * @var FileSystemInterface
	 */
	protected FileSystemInterface $_filesystem;
	
	/**
	 * The current parent of this node.
	 * 
	 * @var ?FolderInterface
	 */
	protected ?FolderInterface $_parent = null;
	
	/**
	 * The current name of the node.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Gets the given node.
	 * 
	 * @param FileSystemInterface $fsys
	 * @param ?FolderInterface $parent
	 * @param string $name
	 * @throws InvalidArgumentException
	 */
	public function __construct(FileSystemInterface $fsys, ?FolderInterface $parent = null, string $name = '')
	{
		$this->_filesystem = $fsys;
		$this->_parent = $parent;
		if(empty($name))
		{
			throw new InvalidArgumentException('The given name should not be empty.');
		}
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->getRealPath();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getFileSystem()
	 */
	public function getFileSystem() : FileSystemInterface
	{
		return $this->_filesystem;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getFileSystemPath()
	 */
	public function getFileSystemPath() : string
	{
		$str = '';
		if(null !== $this->_parent)
		{
			$str = $this->_parent->getFilesystemPath().'/';
		}
		
		return $str.$this->getName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getRealPath()
	 */
	public function getRealPath() : string
	{
		return $this->_filesystem->getAbsolutePath().'/'.$this->getFileSystemPath();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getExtension()
	 */
	public function getExtension() : string
	{
		$pos = \mb_strpos($this->_name, '.', 0, '8bit');
		if(false === $pos)
		{
			return '';
		}
		
		return (string) \mb_substr($this->_name, 0, $pos - 1, '8bit');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getBasename()
	 */
	public function getBasename(?string $suffix = null) : string
	{
		return \basename($this->_name, (string) $suffix);
	}
	
}
