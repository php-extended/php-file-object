<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\File;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use FilesystemIterator;
use InvalidArgumentException;
use Iterator;
use RuntimeException;
use SplFileInfo;
use UnexpectedValueException;

/**
 * Folder class file.
 * 
 * This class is a simple implementation of the FolderInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class Folder extends Node implements FolderInterface
{
	
	/**
	 * The real file representation.
	 * 
	 * @var SplFileInfo
	 */
	protected SplFileInfo $_fileInfo;
	
	/**
	 * Builds a new Folder with the given filesystem, parent folder and name.
	 * 
	 * @param FileSystemInterface $fsys
	 * @param FolderInterface $parent
	 * @param string $name
	 * @throws InvalidArgumentException
	 */
	public function __construct(FileSystemInterface $fsys, ?FolderInterface $parent = null, string $name = '')
	{
		parent::__construct($fsys, $parent, $name);
		$fspath = null === $this->_parent ? $fsys->getAbsolutePath() : $this->_parent->getRealPath();
		$this->_fileInfo = new SplFileInfo($fspath.'/'.$name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getParentFolder()
	 */
	public function getParentFolder() : FolderInterface
	{
		if(null === $this->_parent)
		{
			return $this;
		}
		
		return $this->_parent;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FolderInterface::clearCache()
	 */
	public function clearCache() : bool
	{
		return true;
	}
	
	/**
	 * @return bool
	 */
	public function exists() : bool
	{
		return \is_dir($this->getRealPath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::ensureExists()
	 * @throws RuntimeException
	 */
	public function ensureExists() : bool
	{
		if(!\is_dir($this->getRealPath()))
		{
			if(!\mkdir($this->getRealPath(), 0755, true))
			{
				throw new RuntimeException('Failed to create directory in filesystem '.$this->getFilesystemPath());
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getPermissions()
	 */
	public function getPermissions() : int
	{
		return $this->_fileInfo->getPerms();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getInodeNumber()
	 */
	public function getInodeNumber() : int
	{
		return $this->_fileInfo->getInode();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getSize()
	 */
	public function getSize() : int
	{
		return $this->_fileInfo->getSize();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getOwner()
	 */
	public function getOwner() : int
	{
		return $this->_fileInfo->getOwner();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getGroup()
	 */
	public function getGroup() : int
	{
		return $this->_fileInfo->getGroup();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getATime()
	 * @throws Exception
	 */
	public function getATime() : DateTimeInterface
	{
		return new DateTimeImmutable('@'.((string) $this->_fileInfo->getATime()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getMTime()
	 * @throws Exception
	 */
	public function getMTime() : DateTimeInterface
	{
		return new DateTimeImmutable('@'.((string) $this->_fileInfo->getMTime()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getCTime()
	 * @throws Exception
	 */
	public function getCTime() : DateTimeInterface
	{
		return new DateTimeImmutable('@'.((string) $this->_fileInfo->getCTime()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::isWritable()
	 */
	public function isWritable() : bool
	{
		return $this->_fileInfo->isWritable();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::isReadable()
	 */
	public function isReadable() : bool
	{
		return $this->_fileInfo->isReadable();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::isExecutable()
	 */
	public function isExecutable() : bool
	{
		return $this->_fileInfo->isExecutable();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FolderInterface::listFolders()
	 * @throws UnexpectedValueException
	 */
	public function listFolders() : Iterator
	{
		return new FolderIterator($this->_filesystem, $this, new FilesystemIterator(
			$this->getRealPath(),
			FilesystemIterator::CURRENT_AS_FILEINFO
			| FilesystemIterator::FOLLOW_SYMLINKS
			| FilesystemIterator::KEY_AS_FILENAME
			| FilesystemIterator::SKIP_DOTS
			| FilesystemIterator::UNIX_PATHS,
		));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FolderInterface::listFiles()
	 * @throws UnexpectedValueException
	 */
	public function listFiles() : Iterator
	{
		return new FileIterator($this->_filesystem, $this, new FilesystemIterator(
			$this->getRealPath(),
			FilesystemIterator::CURRENT_AS_FILEINFO
			| FilesystemIterator::FOLLOW_SYMLINKS
			| FilesystemIterator::KEY_AS_FILENAME
			| FilesystemIterator::SKIP_DOTS
			| FilesystemIterator::UNIX_PATHS,
		));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FolderInterface::getFolder()
	 */
	public function getFolder(string $name) : FolderInterface
	{
		return new Folder($this->_filesystem, $this, $name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FolderInterface::getFile()
	 */
	public function getFile(string $relativePath) : FileInterface
	{
		return new File($this->_filesystem, $this, $relativePath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FolderInterface::beVisitedBy()
	 */
	public function beVisitedBy(FilesystemVisitorInterface $visitor)
	{
		return $visitor->visitFolder($this);
	}
	
}
