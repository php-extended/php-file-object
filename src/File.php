<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\File;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use InvalidArgumentException;
use LogicException;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use SplFileObject;

/**
 * File class file.
 *
 * This class is a simple implementation of the FileInterface.
 *
 * @author Anastaszor
 */
class File extends Node implements FileInterface
{
	
	/**
	 * The real file representation.
	 * 
	 * @var ?SplFileObject
	 */
	protected ?SplFileObject $_fileObject = null;
	
	/**
	 * Builds a new Folder with the given filesystem, parent folder and name.
	 * 
	 * @param FileSystemInterface $fsys
	 * @param FolderInterface $parent
	 * @param string $name
	 * @throws InvalidArgumentException
	 */
	public function __construct(FileSystemInterface $fsys, FolderInterface $parent, string $name)
	{
		parent::__construct($fsys, $parent, $name);
	}
	
	/**
	 * Gets the underlying file object.
	 * 
	 * @return SplFileObject
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function getFileObject() : SplFileObject
	{
		if(null === $this->_fileObject)
		{
			$this->ensureExists();
			// modes : https://www.php.net/manual/en/function.fopen.php
			$this->_fileObject = new SplFileObject($this->getRealPath(), 'c+b');
		}
		
		return $this->_fileObject;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getParentFolder()
	 * @psalm-suppress InvalidNullableReturnType
	 * @psalm-suppress NullableReturnStatement
	 */
	public function getParentFolder() : FolderInterface
	{
		/** @phpstan-ignore-next-line */
		return $this->_parent;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::exists()
	 */
	public function exists() : bool
	{
		return \file_exists($this->getRealPath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::ensureExists()
	 */
	public function ensureExists() : bool
	{
		$realPath = $this->getRealPath();
		$dirpath = \dirname($realPath);
		if(!\is_dir($dirpath))
		{
			if(!\mkdir($dirpath, 0755, true))
			{
				throw new RuntimeException('Failed to create directory in filesystem '.$this->getFileSystemPath());
			}
		}
		
		return \touch($realPath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getPermissions()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function getPermissions() : int
	{
		return $this->getFileObject()->getPerms();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getInodeNumber()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function getInodeNumber() : int
	{
		return $this->getFileObject()->getInode();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getSize()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function getSize() : int
	{
		return $this->getFileObject()->getSize();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getOwner()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function getOwner() : int
	{
		return $this->getFileObject()->getOwner();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getGroup()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function getGroup() : int
	{
		return $this->getFileObject()->getGroup();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getATime()
	 * @throws Exception
	 */
	public function getATime() : DateTimeInterface
	{
		return new DateTimeImmutable('@'.((string) $this->getFileObject()->getATime()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getMTime()
	 * @throws Exception
	 */
	public function getMTime() : DateTimeInterface
	{
		return new DateTimeImmutable('@'.((string) $this->getFileObject()->getMTime()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::getCTime()
	 * @throws Exception
	 */
	public function getCTime() : DateTimeInterface
	{
		return new DateTimeImmutable('@'.((string) $this->getFileObject()->getCTime()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::isWritable()
	 */
	public function isWritable() : bool
	{
		return \is_writable($this->getRealPath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::isReadable()
	 */
	public function isReadable() : bool
	{
		return \is_readable($this->getRealPath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\NodeInterface::isExecutable()
	 */
	public function isExecutable() : bool
	{
		return \is_executable($this->getRealPath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileInterface::getFullContents()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function getFullContents() : string
	{
		$usage = \memory_get_usage(true);
		$limit = $this->getMemoryLimit();
		$limit -= 20 * 1024 * 1024;	// assume we need 20Mo of other data
		if($this->getFileObject()->getSize() > $limit - $usage)
		{
			$message = 'The given file {file} is too large for the remaining memory (size: {size}, memory: {limit})';
			$context = ['{file}' => $this->getFileSystemPath(), '{size}' => $this->getFileObject()->getSize(), '{limit}' => $limit];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$data = \file_get_contents($this->getRealPath());
		if(false === $data)
		{
			$message = 'Failed to read data for file at {path}';
			$context = ['{path}' => $this->getRealPath()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileInterface::getDataStream()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function getDataStream() : StreamInterface
	{
		return new FileStream($this->getFileObject());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileInterface::overwrite()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function overwrite(string $rawContents) : int
	{
		$this->getFileObject()->ftruncate(0);
		
		return $this->append($rawContents);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileInterface::overwriteStream()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function overwriteStream(StreamInterface $stream) : int
	{
		$this->getFileObject()->ftruncate(0);
		
		return $this->appendStream($stream);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileInterface::append()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function append(string $rawContents) : int
	{
		if(!$this->getFileObject()->isReadable())
		{
			throw new RuntimeException('This file '.$this->getFileSystemPath().' is not writeable.');
		}
		
		return $this->getFileObject()->fwrite($rawContents);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileInterface::appendStream()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function appendStream(StreamInterface $stream) : int
	{
		if(!$stream->isReadable())
		{
			throw new RuntimeException('The given stream is not readable.');
		}
		
		if(!$this->getFileObject()->isReadable())
		{
			throw new RuntimeException('This file '.$this->getFileSystemPath().' is not writeable.');
		}
		
		$written = 0;
		
		while($stream->eof())
		{
			$written += $this->getFileObject()->fwrite($stream->read(8192));
		}
		
		return $written;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileInterface::beVisitedBy()
	 */
	public function beVisitedBy(FilesystemVisitorInterface $visitor)
	{
		return $visitor->visitFile($this);
	}
	
	/**
	 * @return integer
	 */
	protected function getMemoryLimit() : int
	{
		// assume 128Mo
		$memoryLimit = 128 * 1024 * 1024;
		$memory = (string) \ini_get('memory_limit');
		$matches = [];
		if(\preg_match('/^(\\d+)(.)$/', $memory, $matches))
		{
			/** @phpstan-ignore-next-line */
			if(isset($matches[1], $matches[2]))
			{
				if('M' === $matches[2])
				{
					$memoryLimit = ((int) $matches[1]) * 1024 * 1024; // nnnM -> nnn MB
				}
				if('K' === $matches[2])
				{
					$memoryLimit = ((int) $matches[1]) * 1024; // nnnK -> nnn KB
				}
			}
		}
		
		return (int) $memoryLimit;
	}
	
	/**
	 * Destructor, flushes the file.
	 */
	public function __destruct()
	{
		if(null !== $this->_fileObject)
		{
			$this->_fileObject->fflush();
		}
	}
	
}
