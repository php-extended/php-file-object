<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\File;

use FilesystemIterator;
use FilterIterator;
use InvalidArgumentException;
use Iterator;

/**
 * FolderInterface interface file.
 * 
 * This class represents a new  
 * 
 * @author Anastaszor
 * @implements \Iterator<string, FolderInterface>
 * @extends \FilterIterator<string, FolderInterface, \Iterator<string, FolderInterface>>
 */
class FolderIterator extends FilterIterator implements Iterator
{
	
	/**
	 * The parent filesystem.
	 * 
	 * @var FileSystemInterface
	 */
	protected FileSystemInterface $_filesystem;
	
	/**
	 * This parent folder.
	 * 
	 * @var FolderInterface
	 */
	protected FolderInterface $_parent;
	
	/**
	 * The directory iterator.
	 * 
	 * @var FilesystemIterator
	 */
	protected FilesystemIterator $_iterator;
	
	/**
	 * Builds a new FolderIterator with the given file system, parent folder
	 * and spl filesystem iterator.
	 * 
	 * @param FileSystemInterface $fsys
	 * @param FolderInterface $parent
	 * @param FilesystemIterator $iterator
	 */
	public function __construct(FileSystemInterface $fsys, FolderInterface $parent, FilesystemIterator $iterator)
	{
		$this->_filesystem = $fsys;
		$this->_parent = $parent;
		$this->_iterator = $iterator;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->_parent->getRealPath();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \FilterIterator::accept()
	 */
	public function accept() : bool
	{
		/** $cur \SplFileInfo */
		$cur = parent::current();
		/** @phpstan-ignore-next-line */ /** @psalm-suppress UndefinedInterfaceMethod */
		return (bool) $cur->isDir();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @throws InvalidArgumentException
	 */
	public function current() : FolderInterface
	{
		return new Folder($this->_filesystem, $this->_parent, (string) $this->_iterator->key());
	}
	
}
