<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\File;

use Exception;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use SplFileObject;

/**
 * FileStream interface file.
 *
 * This class represents a stream of data from a file.
 *
 * @author Anastaszor
 */
class FileStream implements StreamInterface
{
	
	/**
	 * The underlying file object.
	 * 
	 * @var ?SplFileObject
	 */
	protected ?SplFileObject $_fileObject;
	
	/**
	 * Builds a new FileStream from the given SplFileObject.
	 * 
	 * @param SplFileObject $object
	 */
	public function __construct(SplFileObject $object)
	{
		$this->_fileObject = $object;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::__toString()
	 */
	public function __toString() : string
	{
		try
		{
			return $this->getContents();
		}
		// @codeCoverageIgnoreStart
		catch(Exception $e)
		{
			return static::class.'@'.\spl_object_hash($this);
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::close()
	 */
	public function close() : void
	{
		$this->_fileObject = null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::detach()
	 */
	public function detach()
	{
		return $this->_fileObject = null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getSize()
	 * @throws RuntimeException
	 */
	public function getSize() : ?int
	{
		if(null === $this->_fileObject)
		{
			return -1;
		}
		
		return $this->_fileObject->getSize();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::tell()
	 */
	public function tell() : int
	{
		if(null === $this->_fileObject)
		{
			return -1;
		}
		
		$ret = $this->_fileObject->ftell();
		if(false === $ret)
		{
			return -1;
		}
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::eof()
	 */
	public function eof() : bool
	{
		if(null === $this->_fileObject)
		{
			return true;
		}
		
		return !$this->_fileObject->valid();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isSeekable()
	 */
	public function isSeekable() : bool
	{
		return null !== $this->_fileObject;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::seek()
	 * @throws RuntimeException
	 */
	public function seek(int $offset, int $whence = \SEEK_SET) : void
	{
		if(null === $this->_fileObject)
		{
			throw new RuntimeException('The stream is closed.');
		}
		
		$ret = $this->_fileObject->fseek($offset, $whence);
		if(-1 === $ret)
		{
			throw new RuntimeException('Failed to seek stream.');
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::rewind()
	 * @throws RuntimeException
	 */
	public function rewind() : void
	{
		if(null === $this->_fileObject)
		{
			throw new RuntimeException('The stream is closed.');
		}
		
		$this->_fileObject->rewind();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isWritable()
	 * @throws RuntimeException
	 */
	public function isWritable() : bool
	{
		if(null === $this->_fileObject)
		{
			throw new RuntimeException('The stream is closed.');
		}
		
		return $this->_fileObject->isWritable();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::write()
	 * @throws RuntimeException
	 */
	public function write(string $string) : int
	{
		if(null === $this->_fileObject)
		{
			throw new RuntimeException('The stream is closed.');
		}
		
		return $this->_fileObject->fwrite($string);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isReadable()
	 * @throws RuntimeException
	 */
	public function isReadable() : bool
	{
		if(null === $this->_fileObject)
		{
			throw new RuntimeException('The stream is closed.');
		}
		
		return $this->_fileObject->isReadable();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::read()
	 * @throws RuntimeException
	 */
	public function read(int $length) : string
	{
		if(null === $this->_fileObject)
		{
			throw new RuntimeException('The stream is closed.');
		}
		
		$ret = $this->_fileObject->fread($length);
		if(false === $ret)
		{
			$message = 'Failed to read data from {path}';
			$context = ['{path}' => $this->_fileObject->getRealPath()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getContents()
	 * @throws RuntimeException
	 */
	public function getContents() : string
	{
		if(null === $this->_fileObject)
		{
			throw new RuntimeException('The stream is closed.');
		}
		
		$ret = $this->_fileObject->fread((int) $this->getSize());
		if(false === $ret)
		{
			$message = 'Failed to read data from {path}';
			$context = ['{path}' => $this->_fileObject->getRealPath()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getMetadata()
	 */
	public function getMetadata(?string $key = null)
	{
		return null;
	}
	
}
