<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\File;

use InvalidArgumentException;

/**
 * FileSystem class file.
 * 
 * This class is a simple implementation of the FileSystemInterface
 * 
 * @author Anastaszor
 */
class FileSystem implements FileSystemInterface
{
	
	/**
	 * The root path of the filesystem.
	 * 
	 * @var string
	 */
	protected string $_absolutePath;
	
	/**
	 * The root folder.
	 * 
	 * @var FolderInterface
	 */
	protected FolderInterface $_rootFolder;
	
	/**
	 * Builds a new FileSystem with the given absolute path which will represent
	 * the root of the new filesystem.
	 * 
	 * @param string $realAbsolutePath
	 * @throws InvalidArgumentException if the absolute path cannot be found
	 */
	public function __construct(string $realAbsolutePath)
	{
		$realAbsolutePath = \realpath($realAbsolutePath);
		if(false === $realAbsolutePath)
		{
			$message = 'Failed to build FileSystem at given path, it does not exists : {path}';
			$context = ['{path}' => $realAbsolutePath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		$this->_absolutePath = $realAbsolutePath;
		$this->_rootFolder = new Folder($this, null, \basename($realAbsolutePath));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->getAbsolutePath();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileSystemInterface::getAbsolutePath()
	 */
	public function getAbsolutePath() : string
	{
		return $this->_absolutePath;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileSystemInterface::getFolder()
	 */
	public function getFolder(string $relativePath) : FolderInterface
	{
		$folder = $this->_rootFolder;
		
		foreach($this->getParts($relativePath) as $pathPart)
		{
			$folder = $folder->getFolder($pathPart);
		}
		
		return $folder;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileSystemInterface::getFile()
	 */
	public function getFile(string $relativePath) : FileInterface
	{
		$fileName = \basename($relativePath);
		$dirName = \dirname($relativePath);
		$folder = $this->_rootFolder;
		
		foreach($this->getParts($dirName) as $pathPart)
		{
			$folder = $folder->getFolder($pathPart);
		}
		
		return $folder->getFile($fileName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\File\FileSystemInterface::beVisitedBy()
	 */
	public function beVisitedBy(FilesystemVisitorInterface $visitor)
	{
		return $visitor->visitFilesystem($this);
	}
	
	/**
	 * The parts of the file path that are sanitized.
	 * 
	 * @param string $path
	 * @return array<integer, string>
	 */
	protected function getParts(string $path) : array
	{
		$parts = \explode('/', $path);
		
		foreach($parts as $k => $part)
		{
			if(empty($part))
			{
				unset($parts[$k]);
			}
			
			if('.' === $part)
			{
				unset($parts[$k]);
			}
			
			if('..' === $part)
			{
				unset($parts[$k]);
				if(0 < $k && isset($parts[$k - 1]))
				{
					unset($parts[$k - 1]);
				}
				
				$parts = \array_values($parts);	// reindex
			}
		}
		
		return \array_values($parts);
	}
	
}
