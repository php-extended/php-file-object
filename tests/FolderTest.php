<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\File\FileSystemInterface;
use PhpExtended\File\Folder;
use PhpExtended\File\FolderInterface;
use PHPUnit\Framework\TestCase;

/**
 * FolderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\File\Folder
 *
 * @internal
 *
 * @small
 */
class FolderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Folder
	 */
	protected Folder $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@//'.__DIR__, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Folder(
			$this->getMockForAbstractClass(FileSystemInterface::class),
			$this->getMockForAbstractClass(FolderInterface::class),
			__DIR__,
		);
	}
	
}
