<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\File\FileStream;
use PHPUnit\Framework\TestCase;

/**
 * FileStreamTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\File\FileStream
 *
 * @internal
 *
 * @small
 */
class FileStreamTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FileStream
	 */
	protected FileStream $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\file_get_contents(__FILE__), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FileStream(new SplFileObject(__FILE__));
	}
	
}
