<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\File\FileSystemInterface;
use PhpExtended\File\FolderInterface;
use PhpExtended\File\FolderIterator;
use PHPUnit\Framework\TestCase;

/**
 * FolderIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\File\FolderIterator
 *
 * @internal
 *
 * @small
 */
class FolderIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FolderIterator
	 */
	protected FolderIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FolderIterator(
			$this->getMockForAbstractClass(FileSystemInterface::class),
			$this->getMockForAbstractClass(FolderInterface::class),
			new FilesystemIterator(__DIR__),
		);
	}
	
}
