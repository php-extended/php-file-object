<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\File\FileSystem;
use PHPUnit\Framework\TestCase;

/**
 * FileSystemTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\File\FileSystem
 *
 * @internal
 *
 * @small
 */
class FileSystemTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FileSystem
	 */
	protected FileSystem $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.__DIR__, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FileSystem(__DIR__);
	}
	
}
