<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-file-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\File\File;
use PhpExtended\File\FileSystemInterface;
use PhpExtended\File\FolderInterface;
use PHPUnit\Framework\TestCase;

/**
 * FileTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\File\File
 *
 * @internal
 *
 * @small
 */
class FileTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var File
	 */
	protected File $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@//'.__DIR__.'/test-file.txt', $this->_object->__toString());
	}
	
	public function testWriteReadWorks() : void
	{
		$this->_object->overwrite("data\x00other");
		
		$filename = __DIR__.'/test-file.txt';
// 		\file_put_contents($filename, "data\x00other");
		$contents = \file_get_contents($filename);
		\unlink($filename);
		
		$this->assertEquals("data\x00other", $contents);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new File(
			$this->getMockForAbstractClass(FileSystemInterface::class),
			$this->getMockForAbstractClass(FolderInterface::class),
			__DIR__.'/test-file.txt',
		);
	}
	
}
