# php-extended/php-file-object

A library that implements the php-extended/php-file-interface interface library.

![coverage](https://gitlab.com/php-extended/php-file-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-file-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-file-object ^8`


## Basic Usage

To use, you should first instanciate a filesystem with the given absolute path
to the real filesystem, which will be used to `chroot` the given files in
the (virtual) filesystem. Then, each file or folder will be added using the
methods `getFolder` and `getFile` of the filesystem object.

```php

use PhpExtended\Filesystem;

/* @var $fs \PhpExtended\File\FileSystem */
$fs = new Filesystem('/dev');	// absolute path from the real fs
/* @var $file \PhpExtended\File\File */
$file = $fs->getFile('random');	// path relative to the virtual fs
/* @var $stream \PhpExtened\File\FileStream */
$stream = $fs->getDataStream();
/* @var $data string */
$data = $stream->read(128);	// 128 bytes read from /dev/random

```


## License

MIT (See [license file](LICENSE)).
